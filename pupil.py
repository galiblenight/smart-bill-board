from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib
# import tensorflow as tf
import numpy as np
import cv2, sys
data = open(sys.argv[1]).readlines()
savefile = sys.argv[2]
num_samples = len(data)

width = 20
height = 10

X = []
y = []
# prepare samples
for i in data:
    i = (i.replace('\n', '')).split(' ')
    filename = i[0]
    centerx = int(i[1])
    centery = int(i[2])
    img = cv2.imread(filename)
    shape = img.shape
    centery = int(height*centery/float(shape[0]))
    centerx = int(width*centerx/float(shape[1]))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    resimg = cv2.resize(gray, (height, width))
    X.append(resimg.astype(np.float64).reshape(-1))
    label = [[0 for k in range(width)] for j in range(height)]
    # label[centery][centerx] = 1
    for j in range(3):
        for k in range(3):
            if centery+j-1 >= 0 and centerx+k-1 >= 0 and centery+j-1 < height and centerx+k-1 < width:
                label[centery+j-1][centerx+k-1] = 1
    # print centerx, centery
    y.append(np.array(label).reshape(-1))

nn = MLPClassifier(solver='lbfgs', learning_rate_init=0.1, alpha=0.4, random_state=5, hidden_layer_sizes=(150, 150))
nn.fit(X, y)
joblib.dump(nn, savefile+'.pkl')
# print nn
print nn.predict(X)[115].reshape([height, width])
print y[115].reshape([height, width])

# x = np.array([[0, 0], [0, 1], [1, 0], [1, 1]], dtype=np.float32)
# y = np.array([[0], [1], [1], [0]], dtype=np.float32)
#
# classifier = tf.contrib.learn.DNNClassifier(hidden_units=[2], n_classes=2, optimizer=tf.train.GradientDescentOptimizer(0.1))
#
# classifier.fit(x=x, y=y, steps=100)
# for i in range(10000):
#     print classifier.predict(x=x)
