import numpy, cv2
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib

look = open('look')
data = look.read()
data = data.split('\n')
X = []
y = []
for i in data[:-1]:
    i = i.split(' ')
    y.append(int(i[-1]))
    X.append([])
    for j in i[:-1]:
        X[-1].append(int(j))
print(len(X))
# print y
# print X, y
#
nn = MLPClassifier(solver='lbfgs', learning_rate_init=0.1, alpha=0.4, random_state=5, hidden_layer_sizes=(100, 100))
nn.fit(X, y)
joblib.dump(nn, 'islook.pkl')
h = nn.predict(numpy.array(X))
hh = ''
print len(h)
for i in h:
    hh += str(i)+' '
print hh
