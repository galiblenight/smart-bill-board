import cv2, dlib, os, sys, numpy as np

data = sys.argv[1]
out_path = sys.argv[2]
out_file = sys.argv[3]

PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"
predictor = dlib.shape_predictor(PREDICTOR_PATH)
face_cascade = cv2.CascadeClassifier('train_cascade/data/frontal/face/19/cascade.xml')

file_lists = open(data).readlines()
# print file_lists
mouseX = 0
mouseY = 0
image = []
eyerect = []
clicked = False

def click_and_crop(event, x, y, flags, param):
    global mouseX, mouseY, clicked
    if event == cv2.EVENT_LBUTTONDOWN:
        clicked = True
        mouseX = x
        mouseY = y
        n_image = np.copy(eyerect)
        cv2.circle(n_image, (x, y), 3, color=(0, 0, 255))
        cv2.imshow('frame', n_image)

if __name__ == "__main__":
    FILE = open(out_file, 'w+')
    k = 0
    for imfile in file_lists:
        clicked = False
        imfile = imfile.replace('\n', '')
        eyeim = cv2.imread('train_cascade/'+imfile)
        # cv2.rectangle(im, (x,y),(x+w,y+h), (255, 0, 255), 2)
        eyeshape = eyeim.shape
        eyerect = cv2.resize(eyeim, dsize=(100, 40))
        cv2.imshow('frame', eyerect)
        cv2.setMouseCallback("frame", click_and_crop)
        push = cv2.waitKey(0) & 0xFF
        if push == ord('q'):
            sys.exit()
        if push == ord(' ') or not clicked:
            continue
        FILE.write(imfile + " " + str(int(round(mouseX*eyeshape[1]/100))+1) + " " + str(int(round(mouseY*eyeshape[0]/40))+1)+"\n")
    FILE.close()















##
