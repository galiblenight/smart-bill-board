import cv2, dlib, os, sys, numpy as np

data = sys.argv[1]
out_path = sys.argv[2]
out_file = sys.argv[3]

PREDICTOR_PATH = "../shape_predictor_68_face_landmarks.dat"
predictor = dlib.shape_predictor(PREDICTOR_PATH)
face_cascade = cv2.CascadeClassifier('../train_cascade/data/frontal/face/19/cascade.xml')

file_lists = open(data).readlines()
mouseX = 0
mouseY = 0
image = []
eyerect = []
clicked = False

def get_landmarks(im):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    rects = face_cascade.detectMultiScale(im, 1.3, 5) # side_cascade
    # for (x,y,w,h) in rects:
    # 	cv2.rectangle(im, (x-w/6,y-h/6),(x+w+w/6,y+h+h/6), (255,148,200), 2)
    rect = []
    for x,y,w,h in rects:
        rect.append(dlib.rectangle(int(x-w/6),int(y-w/6),int(x+w+w/6),int(y+h+w/6)))
        # face = im[y:y+h, x:x+w]

    return [np.matrix([[p.x, p.y] for p in predictor(im, i).parts()]) for i in rect]

def annotate_landmarks(im, landmarks):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    eyepoints = []
    for data in landmarks:
        i = 0
        for idx, point in enumerate(data):
            if i == 36 or i == 42:
                eyepoints.append([])
            if i > 35 and i < 48:
                eyepoints[-1].append([point[0, 0], point[0, 1]])
			# pos = (point[0, 0], point[0, 1])
			# cv2.putText(im, str(i), pos, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255))
			# if i == 1 or i == 8 or i == 15 or (i >= 17 and i <= 26) or i == 30:
			# 	cv2.circle(im, pos, 3, color=(0, 255, 0))
            i += 1
    return eyepoints

def click_and_crop(event, x, y, flags, param):
    global mouseX, mouseY, clicked
    if event == cv2.EVENT_LBUTTONDOWN:
        clicked = True
        mouseX = x
        mouseY = y
        n_image = np.copy(eyerect)
        cv2.circle(n_image, (x, y), 3, color=(0, 0, 255))
        cv2.imshow('frame', n_image)

if __name__ == "__main__":
    FILE = open(out_file, 'w+')
    k = 0
    for imfile in file_lists:
        imfile = imfile.split(' ')
        image = cv2.imread(imfile[0])
        eyepoints = annotate_landmarks(image, get_landmarks(image))
        for eye in eyepoints:
            clicked = False
            x = eye[0][0]
            y = min([eye[1][1], eye[2][1]])
            w = eye[3][0] - x
            h = max([eye[4][1], eye[5][1]]) - y
            # cv2.rectangle(im, (x,y),(x+w,y+h), (255, 0, 255), 2)
            eyeim = image[y+h/16:y+h-h/16, x:x+w]
            eyeshape = eyeim.shape
            eyerect = cv2.resize(eyeim, dsize=(100, 40))
            cv2.imshow('frame', eyerect)
            cv2.setMouseCallback("frame", click_and_crop)
            push = cv2.waitKey(0) & 0xFF
            if push == ord('q'):
                sys.exit()
            if push == ord(' ') or not clicked:
                continue
            FILE.write(out_path+str(k) + ".jpg " + str(int(round(mouseX*eyeshape[1]/100))+1) + " " + str(int(round(mouseY*eyeshape[0]/40))+1)+"\n")
            cv2.imwrite(out_path+str(k)+".jpg", eyeim)
            k += 1
    FILE.close()















##
