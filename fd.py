import numpy as np
import cv2, time
import dlib, os
from sklearn.neural_network import MLPClassifier
from sklearn.externals import joblib

develop = 0

islook = clf = joblib.load('isLook/islook.pkl')
keycode = 0
eye_x = 20
eye_y = 10
os.system("cd /home/$USER/SBB")
os.system("pwd")
PREDICTOR_PATH = "shape_predictor_68_face_landmarks.dat"
predictor = dlib.shape_predictor(PREDICTOR_PATH)
face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
# side_cascade = cv2.CascadeClassifier('train_cascade/data/side/face/2/cascade.xml')
# eye_cascade = cv2.CascadeClassifier('train_cascade/data/frontal/eye/6/cascade.xml')
look = open("look", "w+")
lflag = 0

fname = 0

def adjust_gamma(image, gamma=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	invGamma = 1.0 / gamma
	table = np.array([(pow(i / 255.0, invGamma)) * 255
		for i in np.arange(0, 256)]).astype("uint8")

	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)

def getMeanMat(mat, x, y, w, h):
	initmat = mat[y-h:y+h, x-w:x+w]
	if (initmat.shape[0]*initmat.shape[1]) > 0:
		sumvar = 0
		for i in initmat:
			for j in i:
				sumvar += j
		# print sumvar
		meanmat = sumvar/(initmat.shape[0]*initmat.shape[1])
		# print initmat.shape[0]*initmat.shape[1]
		return meanmat
	return -1

def getSumMat(mat, x, y, w, h):
	initmat = mat[y:y+h, x:x+w]
	return initmat

def nnDetectPupil(im, gray, ep):
	global fname, eye_y, eye_x
	for i in range(2):
		i *= 6
		x = ep[i][0]
		y = min([ep[i+1][1], ep[i+2][1]])
		w = ep[i+3][0] - x
		h = max([ep[i+4][1], ep[i+5][1]]) - y

		x += w/6
		w -= w/6
		# cv2.rectangle(im, (x,y),(x+w,y+h), (255, 0, 255), 2)
		eye = gray[y:y+h, x:x+w]
		cv2.imwrite('train_cascade/deye/'+str(fname)+'.jpg', eye)
		fname+=1
		shape = eye.shape
		eye = cv2.resize(eye, (eye_y, eye_x)).reshape(1, -1)
		output = eye_center_classify.predict_proba(eye)[0]
		maxprop = 0
		pupil = 0
		for k in range(len(output)):
			if maxprop < output[k]:
				maxprop = output[k]
				pupil = k
		ceny = pupil/eye_x
		cenx = pupil%eye_x
		print cenx
		cv2.circle(im, (x+cenx*shape[1]/eye_x, y+ceny*shape[0]/eye_y), 3, color=(0, 0, 255))
		# print output

def detectPupil(im, gray, ep, grid=2):
	center = [[0, 0], [0, 0]]
	for i in range(2):
		try:
			i *= 6
			x = ep[i][0]
			y = min([ep[i+1][1], ep[i+2][1]])
			w = ep[i+3][0] - x
			h = max([ep[i+4][1], ep[i+5][1]]) - y
			# cv2.rectangle(im, (x,y),(x+w,y+h), (255, 0, 255), 2)
			eye = gray[y:y+h, x:x+w]
			# rects = eye_cascade.detectMultiScale(eye, 1.1, 1)
			# for px,py,pw,ph in rects:
			# 	cv2.rectangle(im, (x+px,y+py),(x+px+pw,y+py+ph), (255, 0, 255), 2)
			# eye = contrast(eye, 100, 101, 0, 255)
			div = 0
			dy, dx = eye.shape
			th = 0
			while div == 0 and dy >= grid and dx >= grid:
				threshold = grid*grid*th
				cx = cy = 0
				div = 0
				for j in range(0, dy, grid):
				    for k in range(0, dx, grid):
				        # cv2.circle(image, (j, i), 2, color=(255, 0, 0))
				        item = eye[j:j+grid, k:k+grid]
				        if item.shape[0] != grid or item.shape[1] != grid:
				            continue
				        if item.sum() < threshold:
				            # print 'e'
				            # cv2.circle(image, (j, i), 2, color=(255, 0, 0))
				            cx += k + grid/2
				            cy += j + grid/2
				            div += 1
				# print 'haha'
				if div == 0:
					th += 5
					continue
				cx /= div
				cy /= div
			center[i/6][0] = cx
			center[i/6][1] = cy
			cv2.circle(im, (x+cx, y+cy), 1, color=(0, 0, 255))
		except: pass
	return center
		# cv2.circle(im, (x+minx, y+miny), 3, color=(255, 0, 0))

def adjust_divide(image, div=1.0):
	# build a lookup table mapping the pixel values [0, 255] to
	# their adjusted gamma values
	table = np.array([(i/div > 255 and 255 or i/div) for i in np.arange(0, 256)]).astype("uint8")

	# apply gamma correction using the lookup table
	return cv2.LUT(image, table)

def get_landmarks(im):
	gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
	rects = face_cascade.detectMultiScale(im, 1.2, 5) # side_cascade
	for (x,y,w,h) in rects:
		cv2.rectangle(im, (x,y),(x+w,y+h), (255,148,200), 1)
	rect = []
	for x,y,w,h in rects:
		rect.append(dlib.rectangle(int(x),int(y),int(x+w),int(y+h)))
		# face = im[y:y+h, x:x+w]

	return [np.matrix([[p.x, p.y] for p in predictor(im, i).parts()]) for i in rect]

def annotate_landmarks(im, landmarks):
	global keycode, lflag
	# im = im.copy()
	gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
	for data in landmarks:
		i = 0
		eyepoints = []
		predict = []
		savepoint = ''
		fx = 1000
		fy = 1000
		for idx, point in enumerate(data):
			if fx > point[0, 0]:
				fx = point[0, 0]
			if fy > point[0, 1]:
				fy = point[0, 1]
		for idx, point in enumerate(data):
			if i > 35 and i < 48:
				eyepoints.append([point[0, 0], point[0, 1]])
			pos = (point[0, 0], point[0, 1])
			# cv2.putText(im, str(i), pos, cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 255))
			if i == 1 or i == 8 or i == 15 or (i >= 17 and i <= 26) or i == 30:
				predict.append(point[0, 0] - fx)
				predict.append(point[0, 1] - fy)
				savepoint += str(point[0, 0] - fx) + ' ' + str(point[0, 1] - fy) + ' '
				cv2.circle(im, pos, 1, color=(0, 255, (i==1)*255))
			i+=1
		center = detectPupil(im, gray, eyepoints)

		if develop:
			savepoint += str(center[0][0]) + ' ' + str(center[0][1]) + ' '
			savepoint += str(center[1][0]) + ' ' + str(center[1][1]) + ' '
			if lflag:
				savepoint += '1'
				# cv2.circle(im, (center[0][0], center[0][1]), 3, color=(0, 0, 255))
				# cv2.circle(im, (center[1][0], center[1][1]), 3, color=(0, 0, 255))
			else:
				savepoint += '0'
			print savepoint
			look = open("look", "a+")
			look.write(savepoint+'\n')
			look.close()
		else:
			predict.append(center[0][0])
			predict.append(center[0][1])
			predict.append(center[1][0])
			predict.append(center[1][0])
			l = islook.predict(np.array(predict).reshape(1, -1))
			print l
			if l[0] == 1:
				cv2.putText(im, "look !!", (100, 100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255))
	# return im

def mean(im):
	csum = np.sum(im)
	div = im.shape[0]*im.shape[1]*3
	return csum/div

def contrast(im, a, b, ap, bp):
	lut = []
	f1 = float(ap)/float(a)
	f2 = float(bp-ap)/float(b-a)
	f3 = float(255-bp)/float(255-b)
	for i in range(256):
		if i < a:
			lut.append(i*f1)
		elif i < b:
			lut.append(ap + (i-a) * f2)
		else:
			lut.append(bp + (i-b) * f3)
	table = np.array(lut).astype("uint8")
	return cv2.LUT(im, table)

if __name__ == "__main__":
	cap = cv2.VideoCapture(0)
	fourcc = cv2.VideoWriter_fourcc(*'XVID')
	out = cv2.VideoWriter('test.avi',fourcc, 20.0, (640,480))
	while(True):
        # img = cv2.imread('./image/multi1.jpg')
		ret, frame = cap.read()

		frame = cv2.resize(frame, (160, 120))
		# frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
		# div = mean(frame)/100.0
		# frame = adjust_divide(frame, div)
		# frame = contrast(frame, 20, 21, 50, 205)
		# frame = adjust_gamma(frame, 2.0)

		annotate_landmarks(frame, get_landmarks(frame))
		frame = cv2.resize(frame, (640, 480))
		cv2.imshow('Result', frame)
		out.write(frame)
		keycode = cv2.waitKey(1) & 0xFF
		if keycode == ord('a'):
			lflag = int(not lflag)
		if keycode == ord('q'):
			break
	cap.release()
	out.release()
	cv2.destroyAllWindows()
